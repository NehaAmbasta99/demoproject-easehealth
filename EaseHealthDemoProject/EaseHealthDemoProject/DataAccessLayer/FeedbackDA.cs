﻿using Dapper;
using EaseHealthDemoProject.Areas.Doctor.Models;
using EaseHealthDemoProject.Areas.Patient.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace EaseHealthDemoProject.DataAccessLayer
{
    public class FeedbackDA
    {
        DataContext dataContext = new DataContext();
        public void AddFeedback(FeedbackViewModel feedbackViewModel)
        {
                using (SqlConnection connection = dataContext.connection())
                {
                    connection.Open();
                    var feedbackParam = new { @UserId = feedbackViewModel.UserId , @DoctorId = feedbackViewModel.DoctorName , FeedbackContent = feedbackViewModel.FeedbackContent};                    
                    connection.Execute("PatientAddFeedback", feedbackParam, commandType: CommandType.StoredProcedure);
                }     
        }

        public IEnumerable<FeedbackViewModel> GetDoctorList()
        {
            using(SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                string doctorName = "SELECT UserId AS DoctorId,FullName AS DoctorName FROM UserDetails WHERE RoleId=2";
                var result = connection.Query<FeedbackViewModel>(doctorName);
                return result;
            }
        }

        public List<DoctorFeedbackViewModel> ShowFeedback(int userId)
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var feedbackParam = new { @UserId = userId };                
                List<DoctorFeedbackViewModel> feedbackList = connection.Query<DoctorFeedbackViewModel>("DoctorDisplayFeedback", feedbackParam, commandType: CommandType.StoredProcedure).ToList();
                return feedbackList;
            }
        }
    }
}