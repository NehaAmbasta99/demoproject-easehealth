﻿using Dapper;
using EaseHealthDemoProject.Areas.Main.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace EaseHealthDemoProject.DataAccessLayer
{
    public class LoginDA
    {
        DataContext dataContext = new DataContext();
        public int ValidateLogin(LoginViewModel login)
        {
            using(SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var loginParam = new { @MobileNumber = login.MobileNumber , @Password = login.Hash };                
                int userId = connection.Query<int>("MainLoginCheck", loginParam, commandType: CommandType.StoredProcedure).FirstOrDefault();
                return userId;
            }
                       
        }
        public int GetRole(int userId)
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var roleParam = new { @UserId = userId };                
                int roleId = connection.Query<int>("MainGetUserRole", roleParam, commandType: CommandType.StoredProcedure).FirstOrDefault();
                return roleId;
            }
        }
       

    }
}