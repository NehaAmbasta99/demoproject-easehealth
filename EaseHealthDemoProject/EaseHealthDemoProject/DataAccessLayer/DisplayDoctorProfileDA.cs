﻿using EaseHealthDemoProject.Areas.Doctor.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using System.Web.UI.WebControls;
using System.Web.Mvc;

namespace EaseHealthDemoProject.DataAccessLayer
{
    public class DisplayDoctorProfileDA
    {
        DataContext dataContext = new DataContext();
        public List<DisplayDoctorViewModel> DisplayDoctor(int userId)
        {
            using(SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var doctorParam = new { @UserId  = userId};                
                List<DisplayDoctorViewModel> doctorList = connection.Query<DisplayDoctorViewModel>("DoctorDisplayDetail", doctorParam, commandType: CommandType.StoredProcedure).ToList();
                return doctorList.ToList();
            }
           
        }

        public List<DoctorEditProfileViewModel> DisplayDoctorEditProfile(int userId)
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var doctorParam = new { @UserId = userId};                
                List<DoctorEditProfileViewModel> doctorList = connection.Query<DoctorEditProfileViewModel>("DoctorDisplayDetail", doctorParam, commandType: CommandType.StoredProcedure).ToList();
                RegistrationDA registration = new RegistrationDA();
                doctorList.FirstOrDefault().StateList = new SelectList(registration.GetStateLists(), "StateName", "StateName",doctorList.FirstOrDefault().StateName);
                doctorList.FirstOrDefault().CityList = new SelectList(registration.GetCityLists(), "CityName", "CityName",doctorList.FirstOrDefault().CityName);
                doctorList.FirstOrDefault().LocalityList = new SelectList(registration.GetLocalityLists(), "LocalityName", "LocalityName",doctorList.FirstOrDefault().LocalityName);
                doctorList.FirstOrDefault().QualificationList = new SelectList(registration.GetQualificationList(), "QualificationName", "QualificationName", doctorList.FirstOrDefault().QualificationName);
                doctorList.FirstOrDefault().SpecialityList = new SelectList(registration.GetSpecialityList(), "SpecialityName", "SpecialityName", doctorList.FirstOrDefault().SpecialityName);
                return doctorList.ToList();
            }

        }

    }
}