﻿using EaseHealthDemoProject.Areas.Main.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using System.Data;
using EaseHealthDemoProject.Areas.Doctor.Models;

namespace EaseHealthDemoProject.DataAccessLayer
{
    public class RegistrationDA : DataContext
    {
        DataContext dataContext = new DataContext();
        public IEnumerable<RegistrationViewModel> GetSpecialityList()
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                string speciality = "SELECT SpecialityName FROM Speciality";
                var specialityList = connection.Query<RegistrationViewModel>(speciality);
                return specialityList;
            }
        }

        public IEnumerable<RegistrationViewModel> GetBloodGroupList()
        {
            using(SqlConnection connection = dataContext.connection())
            {
            connection.Open();
            string bloodGroup = "SELECT BloodGroupName FROM BloodGroup";
            var bloodGroupList = connection.Query<RegistrationViewModel>(bloodGroup);
            return bloodGroupList;
            }
        }

        public IEnumerable<RegistrationViewModel> GetQualificationList()
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                string qualification = "SELECT QualificationName FROM Qualification WHERE QualificationId <='4'";
                var qualificationList = connection.Query<RegistrationViewModel>(qualification);
                return qualificationList;
            }
        }

        public IEnumerable<RegistrationViewModel> GetStateLists()
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                string state = "SELECT StateName FROM State";
                var stateList = connection.Query<RegistrationViewModel>(state);
                return stateList;
            }
        }

        public IEnumerable<RegistrationViewModel> GetCityLists()
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                string city = "SELECT CityName FROM City";
                var cityList = connection.Query<RegistrationViewModel>(city);
                return cityList;
            }
        }

        public IEnumerable<RegistrationViewModel> GetLocalityLists()
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                string locality = "SELECT LocalityName FROM Locality";
                var localityList = connection.Query<RegistrationViewModel>(locality);
                return localityList;
            }
        }

        public int AddUser(RegistrationViewModel registerViewModel)
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var registerParam = new
                {
                    @FullName = registerViewModel.FullName,
                    @MobileNumber = registerViewModel.MobileNumber,
                    @StateName = registerViewModel.StateName,
                    @CityName = registerViewModel.CityName,
                    @LocalityName = registerViewModel.LocalityName,
                    @Password = registerViewModel.Hash,
                    @Pincode = registerViewModel.Pincode,
                    @User = registerViewModel.User,
                    @LicenseNumber = registerViewModel.LicenseNumber,
                    @QualificationName = registerViewModel.QualificationName,
                    @SpecialityName = registerViewModel.SpecialityName,
                    @BloodGroupName = registerViewModel.BloodGroupName,
                    @ClinicRegistrationNumber = registerViewModel.ClinicRegistrationNumber,
                    @Experience = registerViewModel.Experience,
                    @Gender = registerViewModel.Gender,
                    @DateOfBirth = registerViewModel.DateOfBirth,
                    @SecondaryPhoneNumber = registerViewModel.SecondaryPhoneNumber
                };
                var userExists = connection.Execute("MainUserAddDetail", registerParam, commandType: CommandType.StoredProcedure);
                return userExists;
            }
        }
        public void EditDoctorDetails(DoctorEditProfileViewModel doctorEditProfileViewModel)
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var editProfileParam = new DynamicParameters();
                editProfileParam.Add("@UserId", doctorEditProfileViewModel.UserId);
                editProfileParam.Add("@FullName", doctorEditProfileViewModel.FullName);
                editProfileParam.Add("@MobileNumber", doctorEditProfileViewModel.MobileNumber);
                editProfileParam.Add("@StateName", doctorEditProfileViewModel.StateName);
                editProfileParam.Add("@CityName", doctorEditProfileViewModel.CityName);
                editProfileParam.Add("@LocalityName", doctorEditProfileViewModel.LocalityName);
                editProfileParam.Add("@Pincode", doctorEditProfileViewModel.Pincode);
                editProfileParam.Add("@LicenseNumber", doctorEditProfileViewModel.LicenseNumber);
                editProfileParam.Add("@QualificationName", doctorEditProfileViewModel.QualificationName);
                editProfileParam.Add("@SpecialityName", doctorEditProfileViewModel.SpecialityName);
                editProfileParam.Add("@ClinicRegistrationNumber", doctorEditProfileViewModel.ClinicRegistrationNumber);
                editProfileParam.Add("@Experience", doctorEditProfileViewModel.Experience);
                connection.Execute("DoctorEditProfile", editProfileParam, commandType: CommandType.StoredProcedure);
            }
        }

        public IEnumerable<QualificationViewModel> AddQualification(string qualificationName)
        {
            List<QualificationViewModel> list = new List<QualificationViewModel>();
            list.Add(new QualificationViewModel() { QualificationName = qualificationName });
            return list;
           
        }
        public void AddDoctorQualification(QualificationViewModel qualification,RegistrationViewModel register)
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();                
                List<QualificationViewModel> list = new List<QualificationViewModel>();
                foreach (var element in list)
                {
                    var qualificationParam = new { @QualificationName = qualification.QualificationName};
                    connection.Execute("DoctorAddQualification", qualificationParam, commandType: CommandType.StoredProcedure);
                }
            }
            
            
        }

    }
}