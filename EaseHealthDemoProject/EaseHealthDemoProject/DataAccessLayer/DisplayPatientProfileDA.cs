﻿using EaseHealthDemoProject.Areas.Patient.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;

namespace EaseHealthDemoProject.DataAccessLayer
{
    public class DisplayPatientProfileDA
    {
        DataContext dataContext = new DataContext();
        public List<DisplayPatientViewModel> DisplayPatient(int userId)
        {
            using(SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var patientParam = new { @UserId =  userId};                
                List<DisplayPatientViewModel> patientList = connection.Query<DisplayPatientViewModel>("PatientDisplayDetail", patientParam, commandType: CommandType.StoredProcedure).ToList();
                return patientList;
            }
           
        }
    }
}