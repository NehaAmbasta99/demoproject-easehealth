﻿using Dapper;
using EaseHealthDemoProject.Areas.Doctor.Models;
using EaseHealthDemoProject.Areas.Patient.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


namespace EaseHealthDemoProject.DataAccessLayer
{
    public class VisitDA
    {
        DataContext dataContext = new DataContext();
        public IEnumerable<DoctorVisitViewModel> GetPatientList()
        {
                using (SqlConnection connection = dataContext.connection())
                {
                    connection.Open();
                    string patientName = "SELECT UserId as PatientId,FullName as PatientName FROM UserDetails WHERE RoleId = 3";
                    var patientNameList = connection.Query<DoctorVisitViewModel>(patientName);
                    return patientNameList;
                }
        }
        public void AddVisit(DoctorVisitViewModel addVisit)
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var visitParam = new
                {
                    @VisitName = addVisit.VisitName,
                    @VisitDate = addVisit.VisitDate,
                    @PatientId = addVisit.PatientName,
                    @UserId = addVisit.UserId
                };
                
                connection.Execute("DoctorAddVisit", visitParam, commandType: CommandType.StoredProcedure);
            }           
        }
        public void AddVisitDetails(DoctorVisitViewModel doctorVisit)
        {
           using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var visitDetailsParam = new
                {
                    @VisitDetails = doctorVisit.VisitDetails,
                    @VisitId = doctorVisit.VisitId
                };
                connection.Execute("DoctorAddVisitDetails", visitDetailsParam, commandType: CommandType.StoredProcedure);
            }            
        }

        public void AddInvoiceDetails(DoctorInvoiceViewModel addInvoice)
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var invoiceParam = new { 
                @TotalAmount = addInvoice.TotalAmount,
                @InvoiceId = addInvoice.VisitId
                };
                connection.Execute("DoctorAddInvoiceDetails", invoiceParam, commandType: CommandType.StoredProcedure);
            }

        }

        public List<DoctorVisitViewModel> DoctorVisitDetails(int userId)
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var doctorVisitParam = new { @UserId = userId};
                List<DoctorVisitViewModel> visitlist = connection.Query<DoctorVisitViewModel>("DoctorDisplayVisit", doctorVisitParam, commandType: CommandType.StoredProcedure).ToList();
                return visitlist;
            }
        }

        public string DoctorDisplayVisitDetails(int visitId)
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();              
                string visitDetails = "SELECT VisitDetails FROM Visits WHERE VisitId ='" + visitId + "'";
                visitDetails = connection.Query<string>(visitDetails).FirstOrDefault();
                return visitDetails;

            }
        }

        public List<DoctorInvoiceViewModel> DoctorAddInvoiceDetails(int userId)
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var invoiceParam = new { @UserId = userId };
                List<DoctorInvoiceViewModel> invoicelist = connection.Query<DoctorInvoiceViewModel>("DoctorAddInvoice", invoiceParam, commandType: CommandType.StoredProcedure).ToList();
                return invoicelist;
            }

        }

        public List<PatientVisitViewModel> PatientVisitDetails(int userId)
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var visitParam = new { @UserId = userId };                
                List<PatientVisitViewModel> visitlist = connection.Query<PatientVisitViewModel>("PatientDisplayVisit", visitParam, commandType: CommandType.StoredProcedure).ToList();
                return visitlist;
            }
        }

        public DoctorInvoiceViewModel DoctorInvoiceDetails(int invoiceId)
        {
            using (SqlConnection connection = dataContext.connection())
            {
                DoctorInvoiceViewModel doctorInvoiceViewModel = new DoctorInvoiceViewModel();            
                connection.Open();
                string query = "SELECT TotalAmount as TotalAmount FROM Invoice WHERE InvoiceId ='" + invoiceId + "'";
                doctorInvoiceViewModel.TotalAmount = connection.Query<float>(query).FirstOrDefault();                
                return doctorInvoiceViewModel;
            }
        }

    }
}