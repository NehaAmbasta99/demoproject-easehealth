﻿using EaseHealthDemoProject.Areas.Doctor.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Dapper;
using System.Data;
using EaseHealthDemoProject.BusinessLayer;

namespace EaseHealthDemoProject.DataAccessLayer
{
    public class AvailabilityDA : DataContext
    {
        DataContext dataContext = new DataContext();

        public List<AvailabilityViewModel> DisplayAvailabilityDetails(int? availabilityId)
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var availabilityParam = new { @UserId = availabilityId };                
                List<AvailabilityViewModel> availabilityList = connection.Query<AvailabilityViewModel>("DoctorDisplayAvailability", availabilityParam, commandType: CommandType.StoredProcedure).ToList();
                return availabilityList;
            }
        }
        public List<AvailabilityViewModel> DisplayAvailabilityBreakDetails(int availabilityId)
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var availabilityParam = new { @AvailabilityId = availabilityId };                
                List<AvailabilityViewModel> availabilityList = connection.Query<AvailabilityViewModel>("DoctorDisplayBreakAvailability", availabilityParam, commandType: CommandType.StoredProcedure).ToList();
                return availabilityList;
            }
        }
        public IEnumerable<AvailabilityViewModel> GetStartTimeList()
        {
           using( SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                string query = "SELECT StartTime FROM StartTime";
                var result = connection.Query<AvailabilityViewModel>(query);
                return result;
            }
           
        }

        public IEnumerable<AvailabilityViewModel> GetEndTimeList()
        {
            using(SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                string query = "SELECT EndTime FROM EndTime";
                var result = connection.Query<AvailabilityViewModel>(query);
                return result;
            }
            
        }
        public void AddAvailabilityDetails(List<AvailabilityBL.AvailabilityDetails> availabilityviewmodel, int userId)
        {
                using (SqlConnection connection = dataContext.connection())
                {
                    connection.Open();                    
                    
                    foreach( var item in availabilityviewmodel)
                    {
                        var availabilityParam = new { @Day = item.SelectedDay, @UserId = userId, @StartTime = item.StartTime, @EndTime = item.EndTime };
                        connection.Execute("DoctorAddAvailability", availabilityParam, commandType: CommandType.StoredProcedure);
                        int i = 0;
                    foreach(var breakitem in item.BreakList)
                    {
                        i++;
                        var availabilityBreakParam = new { @Day = item.SelectedDay , @UserId = userId , @BreakCount = i , @BreakStartTime = breakitem.BreakStartTime , @BreakEndTime = breakitem.BreakCloseTime};
                        connection.Execute("DoctorAddAvailabilityBreak", availabilityBreakParam, commandType: CommandType.StoredProcedure);                       
                    }
                        
                    }
                    
                   
                }
        }


    }
}