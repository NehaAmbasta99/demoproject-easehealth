﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Dapper;
using EaseHealthDemoProject.Areas.Main.Models;
using System.Data.SqlClient;
using System.Data;


namespace EaseHealthDemoProject.DataAccessLayer
{
   public class HomepageDA : DataContext
    {
       DataContext dataContext = new DataContext();
        public IEnumerable<HomepageViewModel> GetStateList()
        {
            using(SqlConnection connection = dataContext.connection())
                {
                connection.Open();
                string state = "SELECT StateId,StateName FROM State";
                var stateList = connection.Query<HomepageViewModel>(state);
                return stateList;
                }
            
        }

        public IEnumerable<HomepageViewModel> GetCityList()
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                string city = "SELECT CityId,CityName FROM City";
                var cityList = connection.Query<HomepageViewModel>(city);
                return cityList;
            }
            
        }
        public IEnumerable<HomepageViewModel> GetLocalityList()
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                string locality = "SELECT LocalityId,LocalityName FROM Locality";
                var localityList = connection.Query<HomepageViewModel>(locality);
                return localityList;
            }
        }
        public IEnumerable<HomepageViewModel> GetSpecialityList()
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                string speciality = "SELECT SpecialityId,SpecialityName FROM Speciality";
                var specialityList = connection.Query<HomepageViewModel>(speciality);
                return specialityList;
            }
        }
        public List<DisplayDoctorViewModel> FindDoctor(HomepageViewModel homepageViewModel)
        {
            using (SqlConnection connection = dataContext.connection())
            {
                connection.Open();
                var doctorParam = new { @StateName = homepageViewModel.StateName , @CityName = homepageViewModel.CityName , @LocalityName = homepageViewModel.LocalityName , @SpecialityName = homepageViewModel.SpecialityName};                
                List<DisplayDoctorViewModel> doctorList = connection.Query<DisplayDoctorViewModel>("MainFindDoctor", doctorParam, commandType: CommandType.StoredProcedure).ToList();
                return doctorList;
            }
        }
        
    }
}
