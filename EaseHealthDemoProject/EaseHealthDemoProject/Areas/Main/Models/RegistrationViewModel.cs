﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EaseHealthDemoProject.Areas.Main.Models
{
    public class RegistrationViewModel
    {
        [Required]
        public string FullName { get; set; }

        [Required]
        [Phone]
        [Key]
        [Display(Name = "MobileNumber")]
        public string MobileNumber { get; set; }


        public int StateId { get; set; }
        [Required]
        public string StateName { get; set; }
        [NotMapped]
        public SelectList StateList { get; set; }


        public int CityId { get; set; }
        [Required]
        public string CityName { get; set; }
        [NotMapped]
        public SelectList CityList { get; set; }


        public int LocalityId { get; set; }
        [Required]
        public string LocalityName { get; set; }
        [NotMapped]
        public SelectList LocalityList { get; set; }

        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [Required]
        public string Password { get; set; }

        [Required]
        public string Pincode { get; set; }

        [Required]
        public string User { get; set; }


        public int QualificationId { get; set; }
        public string QualificationName { get; set; }
        [NotMapped]
        public SelectList QualificationList { get; set; }


        public int SpecialityId { get; set; }
        public string SpecialityName { get; set; }
        [NotMapped]
        public SelectList SpecialityList { get; set; }
        
        public int BloodGroupId { get; set; }
        public string BloodGroupName { get; set; }
        [NotMapped]
        public SelectList BloodGroupList { get; set; }

        public string LicenseNumber { get; set; }

        public string Hash { get; set; }

        public string ClinicRegistrationNumber { get; set; }

        public string Experience { get; set; }

        public string Gender { get; set; }

        public string DateOfBirth { get; set; }

        public string SecondaryPhoneNumber { get; set; }

        public string SelectedQualificationList { get; set; }

    }

    public class QualificationViewModel
    {
       
        public string QualificationName { get; set; }

       
    }
}