﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace EaseHealthDemoProject.Areas.Main.Models
{
            public class LogActionFilter : ActionFilterAttribute
        {
            public override void OnActionExecuting(ActionExecutingContext filterContext)
            {
                Log("OnActionExecuting", filterContext.RouteData);
            }

            public override void OnActionExecuted(ActionExecutedContext filterContext)
            {
                Log("OnActionExecuted", filterContext.RouteData);
            }

            public override void OnResultExecuting(ResultExecutingContext filterContext)
            {
                Log("OnResultExecuting", filterContext.RouteData);
            }

            public override void OnResultExecuted(ResultExecutedContext filterContext)
            {
                Log("OnResultExecuted", filterContext.RouteData);
            }


            private void Log(string methodName, RouteData routeData)
            {
                var controllerName = routeData.Values["controller"];
                var actionName = routeData.Values["action"];
                var message = String.Format("{0} controller:{1} action:{2}", methodName, controllerName, actionName);
                Debug.WriteLine(message, "Action Filter Log");
            }

        }
       //public class CustomAuthorize : ActionFilterAttribute
       // {
       //     public override void OnActionExecuting(AuthorizationContext filterContext)
       //     {
       //         AuthorizeUser(filterContext);
       //     }

       //     public override void OnActionExecuted(AuthorizationContext filterContext)
       //     {
       //         AuthorizeUser(filterContext);
       //     }

       //     public override void OnResultExecuting(AuthorizationContext filterContext)
       //     {
       //         AuthorizeUser(filterContext);
       //     }

       //     public override void OnResultExecuted(AuthorizationContext filterContext)
       //     {
       //         AuthorizeUser(filterContext);
       //     }
       //    private int AuthorizeUser(AuthorizationContext filterContext)
       //         {
       //         if (filterContext.RequestContext.HttpContext != null)
       //             {
       //                 var context = filterContext.RequestContext.HttpContext;
       //                 int roleId = (int)(context.Session["RoleId"]);
       //                 switch(roleId)
       //                 {
       //                     case 1:
       //                     case 2:
       //                     case 3:
       //                         return roleId;
       //                     default:
       //                         throw new ArgumentException("filterContext");
       //                 }
       //             }
       //             throw new ArgumentException("filterContext");
       //           }
           
       // }
    }
