﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EaseHealthDemoProject.Areas.Main.Models
{
    public class HomepageViewModel
    {
         [Key]
        public int StateId { get; set; }
        [Required]
        public string StateName { get; set; }
        [NotMapped]
        public SelectList StateList { get; set; }

        
        public int CityId { get; set; }
        [Required]
        public string CityName { get; set; }
        [NotMapped]
        public SelectList CityList { get; set; }

        
        public int LocalityId { get; set; }
        [Required]
        public string LocalityName { get; set; }
        [NotMapped]
        public SelectList LocalityList { get; set; }

      
        public string SelectedState { get; set; }
       
        public string SelectedCity { get; set; }
    
        public string SelectedLocality { get; set; }
     
        public string SelectedSpeciality { get; set; }

     
        public int SpecialityId { get; set; }
        [Required]
        public string SpecialityName { get; set; }
        [NotMapped]
        public SelectList SpecialityList { get; set; }
        public string Speciality { get; set; }

    }
    }
