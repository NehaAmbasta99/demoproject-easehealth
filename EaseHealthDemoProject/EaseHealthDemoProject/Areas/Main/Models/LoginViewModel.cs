﻿using System.ComponentModel.DataAnnotations;

namespace EaseHealthDemoProject.Areas.Main.Models
{
    public class LoginViewModel
    {
        [Key]
        public int Id { get; set; }

        [Phone]
        [Required]
        public string MobileNumber { get; set; }
        
        [DataType(DataType.Password)]
        [Required]
        public string Password { get; set; }

        public string Hash { get; set; }

        public string RoleName { get; set; }

        public int? RoleId { get; set; }

        public int? UserId { get; set; }

        public int? IsValidUser { get; set; }
    }
}