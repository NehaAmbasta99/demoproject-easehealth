﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EaseHealthDemoProject.Areas.Main.Models
{
    public class DisplayDoctorViewModel
    {

        [Key]
        public int Id { get; set; }
       
        [Display(Name = "Full Name")]
        public string FullName { get; set; }

        [Display(Name = "UserId")]
        public string UserId { get; set; }

        [Display(Name = "Mobile Number")]
        public string MobileNumber { get; set; }

        [Display(Name = "State Name")]
        public string StateName { get; set; }

        [Display(Name = "City Name")]
        public string CityName { get; set; }

        [Display(Name = "Locality Name")]
        public string LocalityName { get; set; }

        [Display(Name = "Pincode")]
        public string Pincode { get; set; }

        [Display(Name = "Qualification Name")]
        public string QualificationName { get; set; }

        [Display(Name = "Speciality Name")]
        public string SpecialityName { get; set; }


        [Display(Name = "Experience")]
        public string Experience { get; set; }

       


    }
}