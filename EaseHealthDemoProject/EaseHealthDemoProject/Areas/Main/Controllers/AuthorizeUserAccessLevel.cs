﻿using EaseHealthDemoProject.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EaseHealthDemoProject.Areas.Main.Controllers
{
    public enum UserRole
    {
        Doctor = 2,
        Patient = 3,
    }
    public class AuthorizeUserAccessLevel : AuthorizeAttribute
    {

        private readonly UserRole[] acceptedRoles;
        public AuthorizeUserAccessLevel(params UserRole[] roles)
        {
            acceptedRoles = roles;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            var currentUser = "";
             if(httpContext.Session["roleId"]!=null)
             {

                 var currentRole = httpContext.Session["roleId"].ToString();
                 if (currentRole != null)
                 {
                     if (currentRole.Equals("2"))
                     {
                         currentUser = "Doctor";
                     }
                     else
                     {
                         currentUser = "Patient";
                     }
                     for (int i = 0; i < acceptedRoles.Length; i++)
                     {
                         if (currentUser.Equals(acceptedRoles[i].ToString()))
                         {
                             return true;
                         }
                     }
                     return false;
                 }
                 else
                 {
                     return false;
                 }
             }
             else
             {
                 return false;
             }
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            base.HandleUnauthorizedRequest(filterContext);
            filterContext.Result = new RedirectResult("~/Main/Home/Unauthorized");
        }
    }
}