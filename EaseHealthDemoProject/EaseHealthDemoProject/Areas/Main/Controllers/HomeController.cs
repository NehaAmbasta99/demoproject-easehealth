﻿using EaseHealthDemoProject.Areas.Main.Models;
using EaseHealthDemoProject.BusinessLayer;
using EaseHealthDemoProject.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EaseHealthDemoProject.Areas.Main.Controllers
{
    [LogActionFilter]
    public class HomeController : Controller
    {

        public ActionResult Index()
        {
            try
            {
                FindDoctorBL home = new FindDoctorBL();
                return View(home.DisplayLocation());
            }
            catch(Exception)
            {
                return View("Error");
            }
            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ListOfDoctors(HomepageViewModel homepageViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    FindDoctorBL findDoctor = new FindDoctorBL();
                    return View(findDoctor.DisplayDoctor(homepageViewModel));
                }
                else
                {
                    return View("Error");
                }
            }
            catch (Exception)
            {
                return View("Error");
            }
        }


        //Login
        public ActionResult HomeLogin()
        {
            try
            {
                return View();
            }
            catch (Exception)
            {
                return View("Error");
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HomeLogin(LoginViewModel login)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    LoginBL user = new LoginBL();
                    var userId = user.ValidateUser(login);
                    if (userId > 0)
                    {
                        Session["userId"] = userId;
                        var roleId = user.GetUserRole(userId);
                        Session["roleId"] = roleId;
                        if (roleId == 2)
                        {
                            return RedirectToAction("DoctorProfile", "Doctor", new { area = "Doctor" });
                        }
                        else
                        {
                            return RedirectToAction("PatientProfile", "Patient", new { area = "Patient" });
                        }
                    }
                    else
                    {
                        return View("HomeLogin");
                    }
                }
                else
                {
                    return View();
                }
            }
            catch (Exception)
            {
                return View("Error");
            }

        }

        //Register
        public ActionResult UserRegister()
        {
            try
            {
                UserBL user = new UserBL();
                return View(user.LoadLists());
            }
            catch (Exception)
            {
                return View("Error");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UserRegister(RegistrationViewModel registerViewModel)
        {
            try
            {
                UserBL user = new UserBL();
                if (ModelState.IsValid)
                {
                    
                    if (registerViewModel.User == "Doctor")
                    {
                        registerViewModel.QualificationName = Session["selectedQualification"].ToString();
                    }
                    int userExists = user.UserRegister(registerViewModel);
                    if (userExists == -1)
                    {
                        ViewBag.Message = "User Exists with the same data kindly recheck your data or register with a different data";
                        return View(user.LoadLists()); ;
                    }
                    else
                    {
                        ViewBag.Message = "Your data is saved successfully";
                        return View(user.LoadLists());
                    }                   
                }
                else
                {                   
                    ViewBag.Message = "Enter data in all fields";
                    return View(user.LoadLists());
                }
            }
            catch (Exception)
            {
                return View("Error");
            }

        }

        [HttpPost]
        public void AddQualification(string value)
        {
            try
            {
                Session["selectedQualification"] = value;
            }
            catch (Exception)
            {

            }

        }
        public ActionResult Error()
        {
            return View();

        }
        public ActionResult Unauthorized()
        {
            return View();
        }

        public ActionResult Logout()
        {
            Session.Remove("userId");
            return Redirect("HomeLogin");
        }
        public ActionResult ListOfDoctors()
        {
            return View("Error");
        }

    }
}