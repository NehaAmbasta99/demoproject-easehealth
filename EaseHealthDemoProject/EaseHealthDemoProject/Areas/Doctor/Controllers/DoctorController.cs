﻿using EaseHealthDemoProject.Areas.Doctor.Models;
using EaseHealthDemoProject.Areas.Main.Controllers;
using EaseHealthDemoProject.Areas.Main.Models;
using EaseHealthDemoProject.BusinessLayer;
using EaseHealthDemoProject.DataAccessLayer;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EaseHealthDemoProject.Areas.Doctor.Controllers
{
    public class DoctorController : Controller
    {
        [AuthorizeUserAccessLevel(UserRole.Doctor)]
        public ActionResult DoctorProfile()
        {
            try
            {
                int userId; 
                int.TryParse((Session["userId"]).ToString(), out userId);
                DoctorBL doctor = new DoctorBL();
                return View(doctor.DisplayDoctor(userId));
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home", new { area = "Main" });
            }
        }

        [AuthorizeUserAccessLevel(UserRole.Doctor)]
        public ActionResult Feedback()
        {
            try
            {
                int userId;
                int.TryParse((Session["userId"]).ToString(), out userId);
                FeedbackBL doctor = new FeedbackBL();
                return View(doctor.DisplayFeedback(userId));
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home", new { area = "Main" });
            }
        }

        [AuthorizeUserAccessLevel(UserRole.Doctor)]
        public ActionResult Availability()
        {
            try
            {
               AvailabilityBL availability = new AvailabilityBL();
                return View(availability.GetTimeList());
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home", new { area = "Main" });
            }
        }
         
        [HttpPost]       
        public ActionResult Availability(string[] availabilityDetails)
        {
            try 
            {
                if (ModelState.IsValid)
                {
                    int userId;
                    int.TryParse((Session["userId"]).ToString(), out userId);
                    AvailabilityBL availability = new AvailabilityBL();
                    AvailabilityViewModel availabilityViewModel = new AvailabilityViewModel();
                    availabilityViewModel.SelectedDayDetails = availabilityDetails;
                    availabilityViewModel.UserId = userId;
                    availability.AddAvailability(availabilityViewModel);
                    return RedirectToAction("Availability");
                }
                else
                    return View();
            }
            catch(Exception)
            {
                return RedirectToAction("Error", "Home", new { area = "Main" });
            }
           
        }
        
        [AuthorizeUserAccessLevel(UserRole.Doctor, UserRole.Patient)]
        public ActionResult DisplayAvailability(int? id)
        {
            try
            {
                if (id != null)
                {
                    AvailabilityBL doctor = new AvailabilityBL();
                    return View(doctor.DisplayDoctorAvailability(id));
                }
                else
                {
                    return RedirectToAction("Unauthorized", "Home", new { area = "Main" });
                }
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home", new { area = "Main" });
            }


        }

        [AuthorizeUserAccessLevel(UserRole.Doctor, UserRole.Patient)]
        public ActionResult DisplayBreakAvailability(int id)
        {
            try
            {
                AvailabilityBL doctor = new AvailabilityBL();
                return View(doctor.DisplayDoctorBreakAvailability(id));
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home", new { area = "Main" });
            }

        }

        [AuthorizeUserAccessLevel(UserRole.Doctor)]
        public ActionResult DoctorVisit()
        {
            try
            {
                int userId;
                int.TryParse((Session["userId"]).ToString(), out userId);
                VisitBL visit = new VisitBL();
                var visitList = visit.DoctorDisplayVisit(userId);
                return View(visitList);
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home", new { area = "Main" });
            }
        }

        [AuthorizeUserAccessLevel(UserRole.Doctor)]
        public ActionResult VisitDetails(int id)
        {
            try
            {
                Session["visitId"] = id;
                VisitBL visit = new VisitBL();
                DoctorVisitViewModel visitDetails = new DoctorVisitViewModel();
                visitDetails.VisitDetails = visit.DoctorDisplayVisitDetails(id);
                return View(visitDetails);
            }
            catch
            {
                return RedirectToAction("Error", "Home", new { area = "Main" });
            }
            
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VisitDetails(DoctorVisitViewModel doctorVisitViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int visitId;
                    int.TryParse((Session["VisitId"]).ToString(), out visitId);
                    VisitBL visit = new VisitBL();
                    doctorVisitViewModel.VisitId = visitId;
                    visit.AddDoctorVisitDetails(doctorVisitViewModel);
                   ViewBag.Message = "Your data has been saved successfully";
                   doctorVisitViewModel.VisitDetails = visit.DoctorDisplayVisitDetails(visitId);
                   return View(doctorVisitViewModel);
                }
                else
                    return View();
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home", new { area = "Main" });
            }
        }

        [AuthorizeUserAccessLevel(UserRole.Doctor)]
        public ActionResult AddVisit()
        {
            try
            {
                DoctorVisitViewModel doctorVisitViewModel = new DoctorVisitViewModel();
                VisitBL addVisit = new VisitBL();
                doctorVisitViewModel = addVisit.GetPatientList();
                return View(doctorVisitViewModel);
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home", new { area = "Main" });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddVisit(DoctorVisitViewModel doctorVisitViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                   
                    int userId;
                    VisitBL visit = new VisitBL();
                    int.TryParse((Session["userId"]).ToString(), out userId);
                    doctorVisitViewModel.UserId = userId;                    
                    visit.AddDoctorVisit(doctorVisitViewModel);
                    ViewBag.Message = "Your data has been saved successfully";
                    VisitBL addVisit = new VisitBL();
                    doctorVisitViewModel=addVisit.GetPatientList();
                    return View(doctorVisitViewModel);
                }
                else
                    return RedirectToAction("Error", "Home", new { area = "Main" });
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home", new { area = "Main" });
            }

        }

        [AuthorizeUserAccessLevel(UserRole.Doctor)]
        public ActionResult AddInvoice(int id)
        {
            try
            {
                DoctorInvoiceViewModel doctorInvoiceViewModel = new DoctorInvoiceViewModel();
                Session["visitId"] = id;
                VisitBL invoice = new VisitBL();
                doctorInvoiceViewModel = invoice.DoctorDisplayInvoice(id);
                return View(doctorInvoiceViewModel);
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home", new { area = "Main" });
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddInvoice(DoctorInvoiceViewModel doctorInvoiceViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int visitId;
                    int.TryParse((Session["visitId"]).ToString(), out visitId);
                    VisitBL invoice = new VisitBL();
                    doctorInvoiceViewModel.VisitId = visitId;
                    invoice.AddInvoice(doctorInvoiceViewModel);
                    ViewBag.Message = "Your data has been saved successfully";
                    doctorInvoiceViewModel = invoice.DoctorDisplayInvoice(visitId);
                    return View(doctorInvoiceViewModel);
                   
                }
                else
                    return View();
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home", new { area = "Main" });
            }
        }

        [AuthorizeUserAccessLevel(UserRole.Doctor)]
        public ActionResult EditProfile()
        {
            try
            {
                int userId;
                int.TryParse((Session["userId"]).ToString(), out userId);
                DoctorEditProfileViewModel editProfileViewModel = new DoctorEditProfileViewModel();
                editProfileViewModel.UserId = userId;
                DoctorBL doctor = new DoctorBL();
                return View(doctor.DisplayDoctorEditProfile(userId));
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home", new { area = "Main" });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProfile(DoctorEditProfileViewModel editProfileViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int userId;
                    int.TryParse((Session["userId"]).ToString(), out userId);
                    DoctorBL doctor = new DoctorBL();
                    editProfileViewModel.QualificationName = Session["selectedQualification"].ToString();
                    editProfileViewModel.UserId = userId;
                    doctor.EditProfile(editProfileViewModel);
                    ViewBag.message = "Your data has been saved successfully";
                    return View(doctor.DisplayDoctorEditProfile(userId));
                }                
            }
               
            catch(Exception)
            {
                return RedirectToAction("Error", "Home", new { area = "Main" });
            }
            return View();
        }

        [AuthorizeUserAccessLevel(UserRole.Doctor)]
        [HttpPost]       
        public void AddQualification(string value)
        {
            try
            {
                Session["selectedQualification"] = value;
            }
            catch (Exception)
            {

            }

        }
    }
}