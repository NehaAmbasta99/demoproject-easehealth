﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EaseHealthDemoProject.Areas.Doctor.Models
{
    public class DoctorVisitViewModel
    {
        public int VisitId { get; set; }

        [Display(Name = "Visit Name")]
        
        public string VisitName { get; set; }

        public int PatientId { get; set; }

        [Display(Name = "Patient Name")]
        
        public string PatientName { get; set; }
        
        [NotMapped]
        public SelectList PatientList { get; set; }

        public int UserId { get; set; }

        [Display(Name="Visit Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")] 
        [DataType(DataType.Date)]       
        public string VisitDate { get; set; }

        [Display(Name= "Visit Details")]       
        public string VisitDetails { get; set; }

        public float TotalAmount { get; set; }
    }
}