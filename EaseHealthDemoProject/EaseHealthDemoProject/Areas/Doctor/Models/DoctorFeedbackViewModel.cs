﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EaseHealthDemoProject.Areas.Doctor.Models
{
    public class DoctorFeedbackViewModel
    {
        
        public int FeedbackId { get; set; }

        public int UserId { get; set; }

        public string Sender { get; set; }

        public string Feedback { get; set; }
    }
}