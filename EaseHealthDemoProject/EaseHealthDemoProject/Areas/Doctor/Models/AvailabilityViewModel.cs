﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EaseHealthDemoProject.Areas.Doctor.Models
{
    public class AvailabilityViewModel
    {
        public int UserId { get; set; }
        [Key]
        public bool Day { get; set; }

        public string SelectedDay { get; set; }

        public int StartTimeId { get; set; }
       
        public TimeSpan StartTime { get; set; }

        public SelectList StartTimeList { get; set; }

        public int EndTimeId { get; set; }

        public TimeSpan EndTime { get; set; }

        [NotMapped]
        public SelectList EndTimeList { get; set; }

        public TimeSpan BreakStartTime { get; set; }

        public TimeSpan BreakCloseTime { get; set; }

        public string[] SelectedDayDetails { get; set; }

        public int AvailabilityId { get; set; }

    }
}