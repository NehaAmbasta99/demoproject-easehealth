﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EaseHealthDemoProject.Areas.Doctor.Models
{
    public class DoctorInvoiceViewModel
    {
        
        

        public int UserId { get; set; }

        public int VisitId { get; set; }

        
        [Display(Name = "InvoiceId")]
        [Required]
        public int InvoiceId { get; set; }      

        [Required]
        [Display(Name = "Total Amount")]
        public float TotalAmount { get; set; }
    }
}