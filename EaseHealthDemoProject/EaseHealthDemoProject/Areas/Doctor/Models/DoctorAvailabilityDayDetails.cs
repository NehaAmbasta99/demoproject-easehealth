﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EaseHealthDemoProject.Areas.Doctor.Models
{
    public class DoctorAvailabilityDayDetails
    {
       public class AvailabilityDetails
        {
            public string SelectedDay { get; set; }
            public string StartTime { get; set; }
            public string EndTime { get; set; }
            public List<Break> BreakList { get; set; }
        }
        public class Break
        {
            public string BreakStartTime { get; set; }
            public string BreakCloseTime { get; set; }
        }
    }
}
