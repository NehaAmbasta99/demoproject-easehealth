﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EaseHealthDemoProject.Areas.Doctor.Models
{
    public class DoctorEditProfileViewModel
    {
        public int UserId { get; set; }
        public string FullName { get; set; }

        [Phone]
        [Key]
        [Display(Name = "MobileNumber")]
        public string MobileNumber { get; set; }


        public int StateId { get; set; }
        public string StateName { get; set; }
        [NotMapped]
        public SelectList StateList { get; set; }


        public int CityId { get; set; }
        public string CityName { get; set; }
        [NotMapped]
        public SelectList CityList { get; set; }


        public int LocalityId { get; set; }
        public string LocalityName { get; set; }
        [NotMapped]
        public SelectList LocalityList { get; set; }

        public string Pincode { get; set; }

        
        public int QualificationId { get; set; }
        public string QualificationName { get; set; }
        public SelectList QualificationList { get; set; }


        public int SpecialityId { get; set; }
        public string SpecialityName { get; set; }
        [NotMapped]
        public SelectList SpecialityList { get; set; }

        public string LicenseNumber { get; set; }

        public string ClinicRegistrationNumber { get; set; }

        public string Experience { get; set; }
        

        public string SelectedQualificationList { get; set; }
    }
}