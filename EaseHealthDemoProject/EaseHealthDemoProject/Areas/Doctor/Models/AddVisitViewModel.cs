﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EaseHealthDemoProject.Areas.Doctor.Models
{
    public class AddVisitViewModel
    {
        [Key]
        public int VisitId { get; set; }

        public int UserId { get; set; }

        public string VisitName { get; set; }

        public DateTime VisitDate { get; set; }
    }
}