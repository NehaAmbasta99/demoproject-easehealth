﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EaseHealthDemoProject.Areas.Patient.Models
{
    public class DisplayPatientViewModel
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "Full Name")]
        public string FullName { get; set; }

        [Display(Name = "Mobile Number")]
        public string MobileNumber { get; set; }

        [Display(Name = "State Name")]
        public string StateName { get; set; }

        [Display(Name = "City Name")]
        public string CityName { get; set; }

        [Display(Name = "Locality Name")]
        public string LocalityName { get; set; }

        [Display(Name = "Pincode")]
        public string Pincode { get; set; }

        [Display(Name = "Bloodgroup Name")]
        public string BloodGroupName { get; set; }

        public string Gender { get; set; }


        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")] 
        [Display(Name = "Date Of Birth")]
        public string DateOfBirth { get; set; }

        [Display(Name = "Secondary Phone Number")]
        public string SecondaryPhoneNumber { get; set; }

       
    }
}