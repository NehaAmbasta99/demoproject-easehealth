﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EaseHealthDemoProject.Areas.Patient.Models
{
    public class PatientVisitViewModel
    {
        public int VisitId { get; set; }

        [Display(Name = "Visit Name")]
        public string VisitName { get; set; }

        [Display(Name = "Doctor Name")]
        public string DoctorName { get; set; }

        public int DoctorId { get; set; }

        public int PatientId { get; set; }

        public int UserId { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")] 
        [Display(Name = "Visit Date")]
        public string VisitDate { get; set; }

        [Display(Name = "Visit Details")]
        public string VisitDetails { get; set; }

        public float TotalAmount { get; set; }
    }
}