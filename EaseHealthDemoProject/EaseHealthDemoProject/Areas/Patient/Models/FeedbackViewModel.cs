﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace EaseHealthDemoProject.Areas.Patient.Models
{
    public class FeedbackViewModel
    {
        
        public int UserId { get; set; }
        
        public int DoctorId { get; set; }

        [Required]
        [Display(Name = "Doctor Name")]
        public string DoctorName { get; set; }
        
        [NotMapped]
        public SelectList DoctorList { get; set; }

        [Required]
        public string FeedbackContent { get; set; }
    }
}