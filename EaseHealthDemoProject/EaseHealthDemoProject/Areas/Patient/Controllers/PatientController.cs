﻿using EaseHealthDemoProject.Areas.Patient.Models;

using EaseHealthDemoProject.BusinessLayer;
using System.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EaseHealthDemoProject.DataAccessLayer;
using EaseHealthDemoProject.Areas.Main.Controllers;
using System.Web.UI;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;


namespace EaseHealthDemoProject.Areas.Patient.Controllers
{
    public class PatientController : Controller
    {
        [AuthorizeUserAccessLevel(UserRole.Patient)]
        public ActionResult PatientProfile()
        { 
            try
            {
            int userId;           
            int.TryParse((Session["userId"]).ToString(), out userId);
            PatientBL patient = new PatientBL();
            return View(patient.DisplayPatient(userId));
            }
            catch(Exception)
            {
                return RedirectToAction("Error", "Home", new { area = "Main" });                
            }

        }

        [AuthorizeUserAccessLevel(UserRole.Patient)]
        public ActionResult Feedback()
        {
            try
            {
                FeedbackBL feedback = new FeedbackBL();
                return View(feedback.LoadDoctorList());
            }            
            catch(Exception)
            {
                return RedirectToAction("Error", "Home", new { area = "Main" });                
            }

        }
        [HttpPost]
        [ValidateAntiForgeryToken()]
        public ActionResult Feedback(FeedbackViewModel feedbackViewModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int userId;
                    int.TryParse((Session["userId"]).ToString(), out userId);
                    FeedbackBL feedback = new FeedbackBL();
                    feedbackViewModel.UserId = userId;
                    feedback.FeedbackRegister(feedbackViewModel);                    
                    ViewBag.Message = "Your data has been saved successfully";                    
                    return View(feedback.LoadDoctorList());
                }
                return View();
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home", new { area = "Main" });
            }
            
        }

        [AuthorizeUserAccessLevel(UserRole.Patient)]
        public ActionResult Visit()
        {
            try
            {
                int userId;
                int.TryParse((Session["userId"]).ToString(), out userId);
                VisitBL patient = new VisitBL();
                return View(patient.PatientDisplayVisit(userId));
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home", new { area = "Main" });
            }            
        }


        [AuthorizeUserAccessLevel(UserRole.Patient)]
        public ActionResult GeneratePdf()
        {
            try
            {
                int userId;
                int.TryParse((Session["userId"]).ToString(), out userId);
                VisitBL patient = new VisitBL();
                return View(patient.PatientDisplayVisit(userId));
            }
            catch (Exception)
            {
                return RedirectToAction("Error", "Home", new { area = "Main" });
            }      
        } 
    }
}