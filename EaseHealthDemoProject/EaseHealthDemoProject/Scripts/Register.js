﻿//=====================================================================Displaying doctor/patient div after click of radio button
$("#doctor_radio").click(function () {
        $("#doctor_div").show();
        $("#patient_div").hide();
    });
    $("#patient_radio").click(function () {
        $("#doctor_div").hide();
        $("#patient_div").show();
    });

    //=================================================================Login page validation
    $("#mobile_textboxhome").click(function () {
        $("#mobile_textboxhome").val("");
    });
    $("#password_textboxhome").click(function () {
        $("#password_textboxhome").val("");
        $("#password_textboxhome").get(0).type = 'password';
    });
    $("#loginbuttonhome").click(function (check) {
        //Fetching the input value
        var mobileNumber = $("#mobile_textboxhome").val();
        var password = $("#password_textboxhome").val();
        var isValid = 0;
        if (mobileNumber == "" || mobileNumber == null) {
            $("#mobile_textboxhome").css("border-color", "red").css("color", "red").css("font-size", "10px").val("Enter correct Phone number!");
            isValid = 1;
        }
        if (password == "" || password == null) {
            $("#password_textboxhome").css("border-color", "red").css("color", "red").css("font-size", "10px").val("Enter password!");
            $("#password_textboxhome").get(0).type = 'text';
            isValid = 1;
        }
        if (isValid == 1) {
            check.preventDefault();
            return false;
        }

    });


    //====================================================================Register page validation
    $("#password").click(function () {
        $("#password").val("");
        $("#password").get(0).type = 'password';
    });
    $("#fullname").click(function () {
        $("#fullname").val("");
    });
    $("#mobilenumber").click(function () {
        $("#mobilenumber").val("");
    });
    $("#pincode").click(function () {
        $("#pincode").val("");
    });
    $("#license-number").click(function () {
        $("#license-number").val("");
    });
    
    $("#experience_textbox").click(function () {
        $("#experience_textbox").val("");
    });
    $("#clinic-registration-number").click(function () {
        $("#clinic-registration-number").val("");
    });
    $("#dob").click(function () {
        $("#dob").val("");
    });
     
    $("#secondary-phone-number").click(function () {
        $("#secondary-phone-number").val("");
    });
    $("#loginbutton_register").click(function (check) {
        //Fetching the input value
        var fullname = $("#fullname").val();
        var mobileNumber = $("#mobilenumber").val();
        var password = $("#password").val();
        var state = $("#state").val();
        var city = $("#city").val();
        var locality = $("#locality").val();
        var pincode = $("#pincode").val();
        var user = $("input:radio[name='User']:checked").val();
        var gender = $("input:radio[name='Gender']:checked").val();
        var licenseNumber = $("#license-number").val();
        var speciality = $("#speciality_register").val();
        var qualification = $("#combo").val();
        var selectedQualifications = $("#validation-qualification").text();
        var experience = $("#experience_textbox").val();
        var clinicRegistrationNumber = $("#clinic-registration-number").val();
        var dateOfBirth = $("#dob").val();
        var bloodGroup = $("#bloodgroup_register").val();
        var secondaryPhoneNumber = $("#secondary-phone-number").val();
        var expressionOnlyNumbers = /^[0-9]/;
        var expressionOnlyLetters = /^[a-zA-Z]/;
        var isValid = 0;
        $("#validation_address").text("")
        $("#validation_user").text("")
        $("#validation_gender").text("")
        $("#validation_speciality").text("")
        $("#validation_qualification").text("")
        $("#validation_dob").text("")
        $("#validation_bloodgroup").text("")
        // Fullname validation
        if (fullname == "" || fullname == null || fullname.test(expressionOnlyLetters)) {
            $("#fullname").css("border-color", "red").css("color", "red").css("font-size", "8px").val("Enter fullname!");
            isValid = 1;
        }
        // mobile number validation
        if (mobileNumber == "" || mobileNumber == null || mobileNumber.length != 10 || mobileNumber.test(expressionOnlyLetters)) {
            $("#mobilenumber").css("border-color", "red").css("color", "red").css("font-size", "8px").val("Enter Phone number! It should have minimum 10 digits");
            isValid = 1;
        }

        if (password == "" || password == null || password.length < 6) {
            $("#password").css("border-color", "red").css("color", "red").css("font-size", "8px").val("Enter password! It should have minimum 6 characters");
            $("#password").get(0).type = 'text';
            isValid = 1;
        }
        if (state == "" || state == 'State') {
            $("#validation_address").addClass("validation_address").append("Please select state! ")
            isValid = 1;
        }
        if (city == "" || city == 'City') {
            $("#validation_address").addClass("validation_address").append(" Please select city! ")
            isValid = 1;
        }
        if (locality == "" || locality == 'Locality') {
            $("#validation_address").addClass("validation_address").append(" Please select locality!")
            isValid = 1;
        }
        if (pincode == "" || pincode == null || pincode.length != 6) {
            $("#pincode").css("border-color", "red").css("color", "red").css("font-size", "8px").val("Enter pincode! It should contain 6 characters");
            isValid = 1;
        }
        if (!user) {
            $("#validation_user").addClass("validation_user").append(" Please select doctor/patient !")
            isValid = 1;
        }
        if (!gender) {
            $("#validation_gender").addClass("validation_gender").append(" Please select gender!")
            isValid = 1;
        }

        if (user == "Doctor") {
            if (licenseNumber == "" || licenseNumber == null) {
                $("#license-number").css("border-color", "red").css("color", "red").css("font-size", "8px").val("Enter license number!");
                isValid = 1;
            }
            if (speciality == "" || speciality == 'Speciality') {
                $("#validation_speciality").addClass("validation_speciality").append(" Please select speciality!")
                isValid = 1;
            }
            if (qualification == "" || qualification == null) {
                $("#validation_qualification").addClass("validation_qualification").append(" Please select qualification!")
                isValid = 1;
            }
            $("#selected-qualification-list").val = $("#validation_qualification").text();


            if (experience == "" || experience == null) {
                $("#experience_textbox").css("border-color", "red").css("color", "red").css("font-size", "8px").val("Enter experience!");
                isValid = 1;
            }
            if (clinicRegistrationNumber == "" || clinicRegistrationNumber == null) {
                $("#clinic-registration-number").css("border-color", "red").css("color", "red").css("font-size", "8px").val("Enter clinic registration number!");
                isValid = 1;
            }
        }

        if (user == 'Patient') {
            if (dateOfBirth == "" || dateOfBirth == null) {
                $("#validation_dob").addClass("validation_dob").append(" Please select date of birth!")
                isValid = 1;
            }
            if (bloodGroup == "" || bloodGroup == null) {
                $("#validation_bloodgroup").addClass("validation_bloodgroup").append(" Please select blood group!")
                isValid = 1;
            }
            if (secondaryPhoneNumber == "" || secondaryPhoneNumber == null || secondaryPhoneNumber.length != 10) {
                $("#secondary-phone-number").css("border-color", "red").css("color", "red").css("font-size", "8px").val("Enter!It should contain minimum 10 digits");
                isValid = 1;
            }

        }

        if (isValid == 1) {
            check.preventDefault();
            return false;
        }
    });
    //==s=====================================================================homepage validation    
    $("#index_submit").click(function (check) {
        //Fetching the input value        
        var state = $("#state").val();
        var city = $("#city").val();
        var locality = $("#locality").val();
        var speciality = $("#speciality").val();
        var error = $("#validation_address").html()
        var isValid = 0;
        $("#validation_address").text("")
        if (state == "" || state == 'State') {
            $("#validation_address").addClass("validation_address_home").append("Please select state! ")
            isValid = 1;
        }
        if (city == "" || city == 'City') {
            $("#validation_address").addClass("validation_address_home").append(" Please select city! ")
            isValid = 1;
        }
        if (locality == "" || locality == 'Locality') {
            $("#validation_address").addClass("validation_address_home").append(" Please select locality!")
            isValid = 1;
        }
        if (speciality == "" || speciality == 'Speciality') {
            $("#validation_address").addClass("validation_address_home").append(" Please select speciality!")
            isValid = 1;
        }
        if (isValid == 1) {
            check.preventDefault();
            resetForm();
            return false;
        }



    });



    //=======================================================================AddInvoice

    $(".submit_invoice").click(function (check) {
        //Fetching the input value
        var amount = $(".right_textbox_invoice").val();
        $(".right_textbox_invoice").click(function () {
            $(".right_textbox_invoice").val("");
        });
        var isValid = 0;
        if (amount == "" || amount == null) {
            $(".right_textbox_invoice").css("border-color", "red").css("color", "red").css("font-size", "10px").val("Enter valid amount!");
            isValid = 1;
        }
        if (isValid == 1) {
            check.preventDefault();
            return false;
        }
    });
    //==============================================================================AddVisit
    $(".loginbutton_addvisit").click(function (check) {
        //Fetching the input value       
        var visitName = $(".addvisitname").val();
        var patientName = $(".addvisitpatient").val();
        var dateOfVisit = $(".addvisitdate").val();
        $(".addvisitname").click(function () {
            $(".addvisitname").val("");
        });
        $(".addvisitdate").click(function () {
            $(".addvisitdate").val("");
        });    
        var isValid = 0;
        if (visitName == "" || visitName == null) {
            $(".addvisitname").css("border-color", "red").css("color", "red").css("font-size", "10px").val("Enter Visit Name!");
            isValid = 1;
        }
        if (patientName == "" || patientName == null || patientName == 'Patient') {
            $("#patientname_validation").addClass("patientname_validation").append("Enter Patient Name!");
            isValid = 1;
        }
        if (dateOfVisit == "" || dateOfVisit == null) {
            $("#dov_validation").addClass("dov_validation").append(" Please select date of visit!")
            isValid = 1;
        }
        if (isValid == 1) {
            check.preventDefault();
            return false;
        }
    });
    //=================================================================VisitDetails
    $("#loginbutton_soap").click(function (check) {
        //Fetching the input value
        var visitDetails = $("#reciever_textbox").val();
        $("#reciever_textbox").click(function () {
            $("#reciever_textbox").val("");
        });
        var isValid = 0;
        if (visitDetails == "" || visitDetails == null) {
            $("#reciever_textbox").css("border-color", "red").css("color", "red").css("font-size", "10px").val("Enter visit details!");
            isValid = 1;
        }
        if (isValid == 1) {
            check.preventDefault();
            return false;
        }
    });
    //=====================================================================feedback_submitbutton
    $(".feedback_submitbutton").click(function (check) {
        //Fetching the input value
        var feedback = $(".feedbackcontent").val();
        var doctor = $(".doctor").val();
        $(".feedbackcontent").click(function () {
            $(".feedbackcontent").val("");
        });
        var isValid = 0;
        if (feedback == "" || feedback == null) {
            $(".feedbackcontent").css("border-color", "red").css("color", "red").css("font-size", "10px").val("Enter visit details!");
            isValid = 1;
        }
        if (doctor == "" || doctor == null || doctor == 'Doctor') {
            $("#doctor_validation").addClass("doctor_validation").append("Select any doctor!");
            isValid = 1;
        }
        if (isValid == 1) {
            check.preventDefault();
            return false;
        }
    });

    //======================================================================Adding the breaks in availability 
    $('.add-break-dropdown').click(function () {
        if ($(this).data('count')) { // already been clicked
            $(this).data('count', $(this).data('count') + 1); // add one
        } else { // first click
            $(this).data('count', 1); // initialize the count  
        }
        var countClick = $(this).data('count');
        var index = countClick;
        var $newDiv = $("<div> </div>");
        var parentDiv = $(this).closest("div").parent().parent();
        var breakDiv = $(this).closest("div").find(".break-container");       
        var id = $(this).closest("div").parent().parent().attr("id");
        var newBreakId = id + "break" + index;        
        var divHeight = parentDiv.height();
        divHeight = divHeight + 60;
        parentDiv.css("height", divHeight);
        breaks = $(this).closest("div").html();
        breakDiv.attr("id", newBreakId);
        breakDiv.css("margin-left", "-16px").html(breaks);
        breakDiv.closest("div").find(".add-break-dropdown").remove();
        breakDiv.append("<br /><br />");
    });
    
//==============================================================Adding Availability
    
    $("#save-day-details").click(function () {
        var numberOfDaysSelected = 0;
        var availability = {}
        var availabilityDetails = {}
        var time = [];
        var breakTime = [];
        var availabilityStringified = [];        
        $("input:checkbox[name='Day']:checked").each(function () {
            numberOfDaysSelected++;
        });       
        $("input:checkbox[name='Day']:checked").each(function () {            
            var selectedDay = $(this).attr("id");
            var breakId = $(this).closest('div').parent().attr("id");
                var startTime = $('#' + selectedDay).closest('div').parent().find(".time-div").find(".starttime").val();
                var endTime = $('#' + selectedDay).closest('div').parent().find(".time-div").find(".endtime").val();
                var breakStartTime = $('#' + selectedDay).closest('div').parent().find(".break-div").find(".breakstarttime").val();
                var breakEndTime = $('#' + selectedDay).closest('div').parent().find(".break-div").find(".breakendtime").val();                               
                availabilityDetails["day"] = selectedDay;
                time.push(startTime);
                time.push(endTime);
                availabilityDetails["time"] = time;
                breakTime.push(breakStartTime);
                breakTime.push(breakEndTime);
                var numberOfBreaks = $('#' + selectedDay).closest('div').parent().find(".break-div").find(".add-break-dropdown").data('count');                                               
                if (numberOfBreaks > 0)
                {
                    for (i = numberOfBreaks ; i >=1; i--) {
                        var extraBreakStartTime = $('#' + selectedDay).closest('div').parent().find(".break-div").find('#' + breakId + 'break' + i).find(".breakstarttime").val();
                        var extraBreakEndTime = $('#' + selectedDay).closest('div').parent().find(".break-div").find('#' + breakId + 'break' + i).find(".breakendtime").val();                       
                        breakTime.push(extraBreakStartTime);
                        breakTime.push(extraBreakEndTime);
                    }
                }
                availabilityDetails["breakTime"] = breakTime;
                availability["dayDetails"] = availabilityDetails;
                availabilityStringified.push(JSON.stringify(availability));
                breakTime=[];
                time = [];
        });
        var availabilityData = availabilityStringified;        
        $.ajax({
            url: '/Doctor/Doctor/Availability',
            data: { availabilityDetails: availabilityData },
            type: "post",
            async: true,
            success: function (result) {
                $(".error-notice_availability").show();
                $(".error-notice_availability").delay(500).fadeOut()

            },
            error: function () {
                               }
        });
    });
    $(".error-notice_availability").hide();
    var selectedQual = "";
    
    
    //========================================================Adding qualification in a div
    
    $('#button1_register').click(function () {
        var degree = $("#combo").val();
        var $newDiv = $("<div> </div>");
        var $newButton = $("<a></a>");
        $newButton.attr("id", "button_" + degree);
        var $newSpan = $("<span></span>");
        $newButton.get(0).type = "button";
        $newDiv.attr("id", degree);
        $newDiv.attr("name", degree);
        $("#validation_qualification").addClass("alignqualification").append($newDiv);
        selectedQual = degree;
        $newDiv.addClass("selected-degree").append(degree);
        $newButton.addClass("glyphicon-remove-circle").addClass("glyphicon").css("padding", "0px");
        $newButton.attr("id", "remove-qualification");    
        $newDiv.append($newButton);
        $("#remove-qualification").click(function () {            
            var qualificationId = $("#remove-qualification").parent().attr("id");
            var parentId = $('#' + qualificationId).parent().attr("id");
            $('#' + parentId).find('#' + qualificationId).remove();
            $("#combo").append($('<option></option>').val(qualificationId).html(qualificationId));
        });
        $('#combo :selected').remove();
    });
    //===================================================================Export To Pdf
   
    $('#button_exportToPdf').appendTo($('tfoot tr td')).on('hover', function () {
        $(this).css('cursor', 'pointer');
    });    
   
    $('#button_exportToPdf').click(function () {       
        $('<iframe src="GeneratePdf"></iframe>').appendTo('body').hide();
    });
    //======================================================================Adding Qualification
    $('#loginbutton_qualification').click(function () {
        var qualification = $("#validation_qualification").text();
        qualification = qualification.replace(/(\r\n|\n|\r)/gm, "");
        $.ajax({
            url: '/Home/AddQualification',
            data: { value: qualification },
            type: "post",
            success: function (result) {
                $(".error-notice_availability").show();
                $(".error-notice_availability").delay(500).fadeOut()
            },
            error: function () {
            }

        });
    });
    $(".error-notice").delay(500).fadeOut()


   
function removetext() {
    $(this).val=""; 
}
























