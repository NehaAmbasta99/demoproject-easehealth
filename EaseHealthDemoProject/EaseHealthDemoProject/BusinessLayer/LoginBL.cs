﻿using EaseHealthDemoProject.Areas.Main.Models;
using EaseHealthDemoProject.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace EaseHealthDemoProject.BusinessLayer
{
    public class LoginBL
    {
        public int ValidateUser(LoginViewModel loginViewModel)
        {
            string password = loginViewModel.Password;
            loginViewModel.Hash = sha256(password);
            LoginDA login = new LoginDA();
            int userId = login.ValidateLogin(loginViewModel);
            return userId;      
        }

       static string sha256(string password)
       {
           System.Security.Cryptography.SHA256Managed crypt = new System.Security.Cryptography.SHA256Managed();
           System.Text.StringBuilder hash = new System.Text.StringBuilder();
           byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(password), 0, Encoding.UTF8.GetByteCount(password));
           foreach (byte theByte in crypto)
           {
               hash.Append(theByte.ToString("x2"));
           }
           return hash.ToString();
       }

       public int GetUserRole(int UserId)
       {
           LoginDA login = new LoginDA();
           int roleId = login.GetRole(UserId);
           return roleId;   
       }
      
    }
}