﻿using EaseHealthDemoProject.Areas.Doctor.Models;
using EaseHealthDemoProject.Areas.Patient.Models;
using EaseHealthDemoProject.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EaseHealthDemoProject.BusinessLayer
{
    
    public class VisitBL
    {
        public DoctorVisitViewModel GetPatientList()
        {
            DoctorVisitViewModel doctorVisitViewModel = new DoctorVisitViewModel();
            VisitDA visit = new VisitDA();
            doctorVisitViewModel.PatientList = new SelectList(visit.GetPatientList(), "PatientId", "PatientName");
            return doctorVisitViewModel;
        }

        public void AddDoctorVisit(DoctorVisitViewModel addVisitViewModel)
        {
            VisitDA visit = new VisitDA();
            visit.AddVisit(addVisitViewModel);
        }
        public List<DoctorVisitViewModel> DoctorDisplayVisit(int userId)
        {
            VisitDA visit = new VisitDA();
            List<DoctorVisitViewModel> visitList = visit.DoctorVisitDetails(userId);
            return visitList.ToList();
        }

        public string DoctorDisplayVisitDetails(int visitId)
        {
            VisitDA visit = new VisitDA();
            string visitDetails = visit.DoctorDisplayVisitDetails(visitId);
            return visitDetails;
        }

        public void AddDoctorVisitDetails(DoctorVisitViewModel addVisitDetails)
        {
            VisitDA visit = new VisitDA();
            visit.AddVisitDetails(addVisitDetails);
        }

        public void AddInvoice(DoctorInvoiceViewModel addInvoice)
        {
            VisitDA visit = new VisitDA();
            visit.AddInvoiceDetails(addInvoice);
        }

        public List<PatientVisitViewModel> PatientDisplayVisit(int userId)
        {
            VisitDA visit = new VisitDA();
            List<PatientVisitViewModel> visitList = visit.PatientVisitDetails(userId);
            return visitList.ToList();
        }      

        public DoctorInvoiceViewModel DoctorDisplayInvoice(int visitId)
        {
            VisitDA visit = new VisitDA();
            DoctorInvoiceViewModel doctorInvoiceViewModel = new DoctorInvoiceViewModel();
            doctorInvoiceViewModel = visit.DoctorInvoiceDetails(visitId);
            return doctorInvoiceViewModel;
        }


    }
}