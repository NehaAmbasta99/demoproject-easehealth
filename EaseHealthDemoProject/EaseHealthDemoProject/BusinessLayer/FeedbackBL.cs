﻿using EaseHealthDemoProject.Areas.Doctor.Models;
using EaseHealthDemoProject.Areas.Patient.Models;
using EaseHealthDemoProject.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EaseHealthDemoProject.BusinessLayer
{
    public class FeedbackBL
    {
        public void FeedbackRegister(FeedbackViewModel feedbackViewModel)
        {
            FeedbackDA feedback = new FeedbackDA();
            feedback.AddFeedback(feedbackViewModel);
        }
       
        public List<DoctorFeedbackViewModel> DisplayFeedback(int userId)
        {
            FeedbackDA feedback = new FeedbackDA();
            List<DoctorFeedbackViewModel> doctor = feedback.ShowFeedback(userId);
            return doctor.ToList();
        }

        public FeedbackViewModel LoadDoctorList()
        {

            FeedbackViewModel feedbackViewModel = new FeedbackViewModel();
            FeedbackDA feedback = new FeedbackDA();
            feedbackViewModel.DoctorList = new SelectList(feedback.GetDoctorList(), "DoctorId", "DoctorName");
            return feedbackViewModel;
        }
    }
}