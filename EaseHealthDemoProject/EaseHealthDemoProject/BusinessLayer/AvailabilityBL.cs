﻿using EaseHealthDemoProject.Areas.Doctor.Models;
using EaseHealthDemoProject.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EaseHealthDemoProject.BusinessLayer
{
    public class AvailabilityBL
    {
        public AvailabilityViewModel GetTimeList()
        {
            AvailabilityViewModel availabilityViewModel = new AvailabilityViewModel();
            AvailabilityDA availability = new AvailabilityDA();
            availabilityViewModel.StartTimeList = new SelectList(availability.GetStartTimeList(), "StartTime", "StartTime");
            availabilityViewModel.EndTimeList = new SelectList(availability.GetEndTimeList(), "EndTime", "EndTime");
            return availabilityViewModel;
        }
        public List<AvailabilityViewModel> DisplayDoctorAvailability(int? availabilityId)
        {
                AvailabilityDA availability = new AvailabilityDA();
                List<AvailabilityViewModel> doctor = availability.DisplayAvailabilityDetails(availabilityId);
                return doctor.ToList();               
            
        }
        public List<AvailabilityViewModel> DisplayDoctorBreakAvailability(int availabilityId)
        {
                AvailabilityDA availability = new AvailabilityDA();
                List<AvailabilityViewModel> doctor = availability.DisplayAvailabilityBreakDetails(availabilityId);
                return doctor.ToList();               
            
        }
        public void AddAvailability(AvailabilityViewModel availabilityViewModel)
        {
            var userId = availabilityViewModel.UserId;
            foreach(var item in availabilityViewModel.SelectedDayDetails)
            {
                List<AvailabilityDetails> dayList = new List<AvailabilityDetails>();
                dayList = GetListOfDayElements(item);                
                AvailabilityDA availability = new AvailabilityDA();
                availability.AddAvailabilityDetails(dayList, userId);
            }
        }
       


        public class AvailabilityDetails
        {
            public int UserId { get; set; }
            public string SelectedDay { get; set; }
            public string StartTime { get; set; }
            public string EndTime { get; set; }
            public List<Break> BreakList { get; set; }

        }
        public class Break
        {
            public string BreakStartTime { get; set; }
            public string BreakCloseTime { get; set; }
        }
        public static List<AvailabilityDetails> GetListOfDayElements(string selectedDay)
        {
            string strModified;
            char[] delim = { ',', '[', ']', '{', '}' };
            int length = selectedDay.Length;
            string[] dayArray = new string[length/6];
            string str = selectedDay;
            strModified = str.Replace("\"", "");
            string[] strArr = strModified.Split(delim);
            int count = 0;
            int i = 0;
            foreach (string s in strArr)
            {
                dayArray[i] = s;
                i++;
                count++;
            }

            for (i = 0; i < dayArray.Length; i++)
            {
                if (dayArray[i] == "" || dayArray[i] ==null|| dayArray[i]=="dayDetails:" || dayArray[i] == "time:"||dayArray[i] =="breakTime:")
                {
                    var numbersList = dayArray.ToList();                    
                        numbersList.Remove(dayArray[i]);
                        i--;
                    
                    dayArray = numbersList.ToArray();
                }
            }
            List<AvailabilityDetails> li = new List<AvailabilityDetails>();
            List<Break> list = new List<Break>();
            for (i = 0; i < dayArray.Length; i++)
            {  
                if (dayArray[i].Substring(0, 4) == "day:")
                {
                    var a = dayArray[i].Substring(0, 4);
                    dayArray[i] = dayArray[i].Replace(a, "");                   
                }
                if (i > 2)
                {
                    if (dayArray[i] != null || dayArray[i] !="")
                    {
                            list.Add(new Break() { BreakStartTime = dayArray[i], BreakCloseTime = dayArray[i+1] });
                            i = i + 1;                        
                    }
                    else break;
                }
            }            
            li.Add(new AvailabilityDetails() { SelectedDay = dayArray[0], StartTime = dayArray[1], EndTime = dayArray[2],BreakList = list});
            return li;
        }
    }
}