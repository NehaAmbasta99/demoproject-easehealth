﻿using EaseHealthDemoProject.Areas.Patient.Models;
using EaseHealthDemoProject.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EaseHealthDemoProject.BusinessLayer
{
    public class PatientBL
    {
        public List<DisplayPatientViewModel> DisplayPatient(int UserId)
        {
            DisplayPatientProfileDA displayPatient = new DisplayPatientProfileDA();
            List<DisplayPatientViewModel> patient = displayPatient.DisplayPatient(UserId);
            return patient.ToList();
        }
    }
}