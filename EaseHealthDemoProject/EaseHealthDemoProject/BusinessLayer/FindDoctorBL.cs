﻿using EaseHealthDemoProject.Areas.Main.Models;
using EaseHealthDemoProject.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EaseHealthDemoProject.BusinessLayer
{
    public class FindDoctorBL
    {
        public List<DisplayDoctorViewModel> DisplayDoctor(HomepageViewModel homepageViewModel)
        {
            HomepageDA home = new HomepageDA();
            List<DisplayDoctorViewModel> list = home.FindDoctor(homepageViewModel);
            return list.ToList();
        }

        public HomepageViewModel DisplayLocation()
        {
            HomepageViewModel homepageViewModel = new HomepageViewModel();
            HomepageDA home = new HomepageDA();
            homepageViewModel.StateList = new SelectList(home.GetStateList(), "StateName", "StateName");
            homepageViewModel.CityList = new SelectList(home.GetCityList(), "CityName", "CityName");
            homepageViewModel.LocalityList = new SelectList(home.GetLocalityList(), "LocalityName", "LocalityName");
            homepageViewModel.SpecialityList = new SelectList(home.GetSpecialityList(), "SpecialityName", "SpecialityName");
            return homepageViewModel;
        }
    }

}