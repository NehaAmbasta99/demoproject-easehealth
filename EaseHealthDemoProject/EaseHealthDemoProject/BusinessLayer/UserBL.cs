﻿using EaseHealthDemoProject.Areas.Main.Models;
using EaseHealthDemoProject.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace EaseHealthDemoProject.BusinessLayer
{
    public class UserBL
    {
        public int UserRegister(RegistrationViewModel registrationViewModel)
        {
            string password = registrationViewModel.Password;
            registrationViewModel.Hash = sha256(password);
            RegistrationDA registration = new RegistrationDA();
            int userExists = registration.AddUser(registrationViewModel);
            return userExists;
        }       
        public RegistrationViewModel LoadLists()
        {
            RegistrationViewModel registrationViewModel = new RegistrationViewModel();
            RegistrationDA registration = new RegistrationDA();
            registrationViewModel.StateList = new SelectList(registration.GetStateLists(), "StateName", "StateName");
            registrationViewModel.CityList = new SelectList(registration.GetCityLists(), "CityName", "CityName");
            registrationViewModel.LocalityList = new SelectList(registration.GetLocalityLists(), "LocalityName", "LocalityName");
            registrationViewModel.QualificationList = new SelectList(registration.GetQualificationList(), "QualificationName", "QualificationName");
            registrationViewModel.SpecialityList = new SelectList(registration.GetSpecialityList(), "SpecialityName", "SpecialityName");
            registrationViewModel.BloodGroupList = new SelectList(registration.GetBloodGroupList(), "BloodGroupName", "BloodGroupName");
            return registrationViewModel;
        }
            
        static string sha256(string password)
        {
            System.Security.Cryptography.SHA256Managed crypt = new System.Security.Cryptography.SHA256Managed();
            System.Text.StringBuilder hash = new System.Text.StringBuilder();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(password), 0, Encoding.UTF8.GetByteCount(password));
            foreach (byte theByte in crypto)
            {
                hash.Append(theByte.ToString("x2"));
            }
            return hash.ToString();
        }
    }
}