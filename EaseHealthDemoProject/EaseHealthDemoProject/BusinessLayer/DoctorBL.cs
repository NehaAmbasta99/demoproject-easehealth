﻿using EaseHealthDemoProject.Areas.Doctor.Models;
using EaseHealthDemoProject.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EaseHealthDemoProject.BusinessLayer
{
    public class DoctorBL
    {
        public List<DisplayDoctorViewModel> DisplayDoctor(int UserId)
        {
            DisplayDoctorProfileDA displayDoctor = new DisplayDoctorProfileDA();
            List<DisplayDoctorViewModel> doctor = displayDoctor.DisplayDoctor(UserId);
            return doctor.ToList();
        }
         public void EditProfile(DoctorEditProfileViewModel editProfileViewModel)
        {
            RegistrationDA registration = new RegistrationDA();
            registration.EditDoctorDetails(editProfileViewModel);
        }

         public List<DoctorEditProfileViewModel> DisplayDoctorEditProfile(int UserId)
         {
             DisplayDoctorProfileDA displayDoctor = new DisplayDoctorProfileDA();
             List<DoctorEditProfileViewModel> doctor = displayDoctor.DisplayDoctorEditProfile(UserId);
             return doctor.ToList();
         }
    }
}