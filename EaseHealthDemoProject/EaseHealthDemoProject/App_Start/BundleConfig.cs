﻿using System.Web;
using System.Web.Optimization;

namespace EaseHealthDemoProject
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"
                        , "~/Scripts/bootstrap-datepicker-master/bootstrap-datepicker.js" , 
                        "~/Scripts/select-master/pqselect.dev.js" , "~/Scripts/select-master/pqselect.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js",
                      "~/Scripts/Register.js"
                      , "~/Scripts/Homepage.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css", "~/Content/Homepage.css", "~/Content/select-master/pqselect.bootstrap.dev.css", "~/Content/select-master/pqselect.bootstrap.min.css", "~/Content/select-master/pqselect.dev.css", "~/Content/select-master/pqselect.min.css"));
        }
    }
}
