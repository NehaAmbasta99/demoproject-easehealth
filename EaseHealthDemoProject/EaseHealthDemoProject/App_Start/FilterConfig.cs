﻿using EaseHealthDemoProject.Areas.Main.Models;
using System.Web;
using System.Web.Mvc;

namespace EaseHealthDemoProject
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new LogActionFilter());
        }
    }
}
